using Godot;
using System;
using System.Collections.Generic;
using Godot.Collections;
using outinthewoods;

namespace outinthewoods
{
	public partial class Faceless : StaticBody3D,IPhotoable
	{
		[Export] public string Title { get; private set; } = "Faceless";
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"no face", "faceless", "blank", "sweater", "fancy", "formal", "slender", "slim", "thin",
				"pullover", "slacks", "human", "humanoid", "tall", "brown", "white", "caucasian", "shirt",
				"tie", "necktie"
				
			},
			new HashSet<string>
			{
				"stare", "stalk", "creep", "face", "whisper", "murmur", "disappear", "stalking", "staring",
				"vanish", "look", "facing", "camera", "harmless", "unsettling", "", "disappearing",
				"vanishing", "looking", "murmuring", "whispering", "creeping"
			},
			new HashSet<string>
			{
				"dusk", "sunset", "sundown", "night", "random", "distance", "evening"
			},
			170,
			230
		);
		
		private Player _player;

		private void OnLookedAt()
		{
			QueueFree();
		}

		public override void _Ready()
		{
			_player = GetTree().Root.GetNode<Player>(World.PlayerPath);
		}

		public override void _PhysicsProcess(double delta)
		{
			Vector3 lookDir = new Vector3
			(
				_player.Position.x,
				Position.y,
				_player.Position.z
			);
			LookAt(lookDir);
		}
	}
}
