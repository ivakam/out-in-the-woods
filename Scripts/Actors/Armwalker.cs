using Godot;
using System.Collections.Generic;
using System.Linq;

namespace outinthewoods
{
	public partial class Armwalker : Actor
	{
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"pale", "flesh", "fleshy", "chubby", "arms", "beak", "no eyes", "without eyes",
				"eyes are missing", "blind", "suction", "cup", "pink", "yellow"
			},
			new HashSet<string>
			{
				"knock", "peck", "hit", "thump", "tree", "walk", "pack", "packs", "many",
				"several", "harmless", "docile", "goofy", "hitting", "pecking",
				"knocking", "thumping", "walking"
			},
			new HashSet<string>
			{
				"dawn", "sunrise", "morning", "packs", "pack", "random", "forest", "trees"
			},
			130,
			170
			);
		
		private bool _pecking = false;
		private StaticBody3D _targetTree;
		private List<StaticBody3D> _treeBlacklist = new List<StaticBody3D>();
		private HashSet<Node3D> _eligibleTrees;

		protected void PeckCallback()
		{
			if (World.Rng.RandiRange(1, 20) > 17)
			{
				_targetTree = null;
			}
			else
			{
				Peck();
			}
		}

		protected void Peck()
		{
			Vector3 lookDir = new Vector3
			(
				_targetTree.GlobalTransform.origin.x,
				GlobalTranslation.y,
				_targetTree.GlobalTransform.origin.z
			);
			LookAt(lookDir);
			
			Anim.Play("Peck");
		}
		
		protected override void AiTick()
		{
			if (!_pecking)
			{
				if (_targetTree == null)
				{
					if (_eligibleTrees.Count == 0)
						return;
					_targetTree = (StaticBody3D) Util.GetClosest(
						GlobalTranslation,
						_eligibleTrees);
					_eligibleTrees.Remove(_targetTree);
					Nav.SetTargetLocation(_targetTree.GlobalTransform.origin);
				}

				if (!Nav.IsTargetReached())
				{
					PositionalMove(Nav.GetNextLocation());
				}
				else
				{
					Peck();
				}
			}
		}

		public override void _Ready()
		{
			base._Ready();
			if (GetParent() is ArmwalkerPack p)
				_eligibleTrees = p.Trees;
			else
				_eligibleTrees = GetTree().GetNodesInGroup("tree").Cast<Node3D>().ToHashSet();
		}
	}
}


