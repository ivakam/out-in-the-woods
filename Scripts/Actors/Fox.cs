using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class Fox : Critter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 5},
				{TimesOfDay.Noon, 5},
				{TimesOfDay.Dusk, 10},
				{TimesOfDay.Night, 30},
			};
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"red", "small", "dog", "hound", "canine", "orange", "tail", "snout", "ear", "pawed", "paws",
				"teeth", "claws"
			},
			new HashSet<string>
			{
				"scurry", "run", "roam", "eat", "hunt", "critter", "bark", "yell", "scream", "noise", "sound",
				"flee", "passive", "docile", "harmless", "nocturnal", "roaming", "screaming", "yelling", "barking",
				"running", "fleeing", "eating", "hunting", "critters"
			},
			new HashSet<string>
			{
				"random", "anywhere", "forest", "night", "dark", "evening", "sunset", "dusk", "moon", "nocturnal"
			},
			40,
			150);
		
		public override void PlayPassiveSound()
		{
			AudioStreamPlayer3D player = GetNode<AudioStreamPlayer3D>("Bark");
			int bark = World.Rng.RandiRange(1, 6);
			AudioStreamMP3 stream = ResourceLoader.Load<AudioStreamMP3>($"res://Assets/Sound/fox/fox{bark}.mp3");
			player.Stream = stream;
			player.Play();
		}
	}
}
