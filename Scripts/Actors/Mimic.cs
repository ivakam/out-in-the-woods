using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class Mimic : Actor
	{
		[Export] public float AttackCooldown { get; private set; } = 3f;
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"worm", "man", "ranger", "male", "human", "bait", "tongue", "string", "appendage", "angler",
				"lure", "large", "teeth", "circle", "flesh", "red"
			},
			new HashSet<string>
			{
				"bait", "lure", "trick", "scream", "help", "yell", "call", "pull", "swallow", "eat", "attack",
				"dangerous", "kill", "harmful", "scary", "dig", "burrow", "bury", "sound", "hear", "distance",
				"baiting", "luring", "tricking", "screaming", "yelling", "calling", "pulling", "swallowing",
				"eating", "killing", "attacking", "burrowing", "digging"
			},
			new HashSet<string>
			{
				"dusk", "sunset", "dawn", "noon", "day", "morning", "evening", "night", "moon", "alone", "isolated",
				"hear", "sound"
			},
			300,
			600
			);
		
		private Player _player;
		private Area3D _hurtbox;
		private bool _activated = false;
		private bool _attacking = false;

		protected override void AiTick()
		{
			if (!_activated)
				return;
			LookAt(_player.GlobalTransform.origin);
			if (!_attacking)
			{
				Anim.Play("Idle");
				if (Util.IsNearLocation(GlobalTranslation, _player.GlobalTransform.origin, 5f))
				{
					Attack();
				}
			}
		}

		private void Attack()
		{
			_attacking = true;
			GetNode<Timer>("AttackCooldown").Start(AttackCooldown);
			Anim.Play("Attack");
		}

		private void Scream()
		{
			GetNode<AudioStreamPlayer3D>("Scream").Play();
			GetNode<Timer>("ScreamTimer").Start(8);
		}

		public override void _Ready()
		{
			Anim = GetNode<AnimationPlayer>("AnimationPlayer");
			_player = GetTree().Root.GetNode<Player>(World.PlayerPath);
			_hurtbox = GetNode<Area3D>("Hurtbox");
		}

		public override void _PhysicsProcess(double delta)
		{
			if (!Visible)
				return;
			
			AiTick();
		}

		private void OnAttack()
		{
			_hurtbox.Monitoring = true;
		}

		private void DisableHurtbox()
		{
			_hurtbox.Monitoring = false;
			Anim.Play("Idle");
		}

		private void OnAttackCooldownTimeout()
		{
			_attacking = false;
		}
		
		private void OnScreamTimeout()
		{
			if (_activated)
				return;
			Scream();
		}

		private void OnActivate(Node3D body)
		{
			if (_activated)
				return;
			Anim.Play("Activate");
		}

		private void OnActivateFinished()
		{
			Anim.Play("Idle");
			_activated = true;
		}
			
		private void HurtboxEntered(Node3D body)
		{
			_player.Die();
		}
	}
}
