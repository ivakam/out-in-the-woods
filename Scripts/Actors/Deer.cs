using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class Deer : Critter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 15},
				{TimesOfDay.Noon, 10},
				{TimesOfDay.Dusk, 20},
				{TimesOfDay.Night, 5},
			};

		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"deer", "red", "brown", "fur", "hooves", "hoof", "medium", "mid", "small", "eyes",
				"four", "4", "legs"
			},
			new HashSet<string>
			{
				"roam", "passive", "critter", "prey", "flee", "run", "alone", "packs", "graze", "grass",
				"hide", "docile", "harmless", "critters", "passively", "grazing", "fleeing", "running",
			},
			new HashSet<string>
			{
				"random", "forest", "all", "dusk", "dawn", "morning", "evening", "day", "roam",
				"anywhere", "randomly"
			},
			140,
			200);
		
		public override void PlayPassiveSound()
		{
		}
	}
}
