using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class Sleeper : StaticBody3D,IPhotoable
	{
		[Export] public string Title { get; private set; } = "Sleeper";
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"pile", "sticks", "debris", "black", "dark", "mass", "ball", "thing"
			},
			new HashSet<string>
			{
				"breathe", "undulate", "pulse", "heave", "sleep", "dormant", "docile",
				"murmur", "snore", "rumble"
			},
			new HashSet<string>
			{
				"clearing", "day", "daytime", "noon", "open", "same", "static"
			},
			250,
			400
			);

		public override void _Ready()
		{
			base._Ready();
			GetNode<AnimationPlayer>("AnimationPlayer").Play("Idle");
		}
	}
}
