using Godot;
using System;

namespace outinthewoods
{
	public abstract partial class Critter : Actor
	{
		public abstract void PlayPassiveSound();

		protected override void OnIdleTimeout()
		{
			base.OnIdleTimeout();
			if (World.Rng.Randf() > 0.995)
				PlayPassiveSound();
		}

		public override void _PhysicsProcess(double delta)
		{
			Vector3 playerPos = GetTree().Root.GetNode<Node3D>(World.PlayerPath).GlobalTransform.origin;
			if (Util.XzDistanceTo(GlobalTransform.origin, playerPos) > 40)
				return;
			base._PhysicsProcess(delta);
		}
	}
}
