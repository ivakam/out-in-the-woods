using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class LadyLichen : Actor
	{
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"pale", "drowned", "drown", "ragged", "tattered", "robe", "demon", "lichen",
				"hair", "bloated", "bloat", "white", "sickly", "bog", "moss", "float", "thin",
				"blue", "green", "speckled"
			},
			new HashSet<string>
			{
				""
			},
			new HashSet<string>
			{
				""
			},
			160,
			220
		);

	}
}
