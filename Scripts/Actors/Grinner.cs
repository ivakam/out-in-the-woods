using Godot;
using System;
using System.Collections.Generic;
using outinthewoods;

namespace outinthewoods
{
	public partial class Grinner : Actor
	{
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"small", "cat", "lemur", "feline", "yellow", "black", "fur", "stilted", "weird", "grin", "smile",
				"creepy", "unsettling", "teeth", "legs", "tail"
			},
			new HashSet<string>
			{
				"packs", "pack", "many", "scurry", "run", "passive", "docile", "cute", "harmless", "not dangerous",
				"roam", "walk", "broken", "running", "walking", "scurrying", "roaming"
			},
			new HashSet<string>
			{
				"night", "midnight", "moon", "dusk", "evening", "sunset", "dawn", "noon", "random", "forest", "pack"
			},
			50,
			150);
		
		protected override void AiTick()
		{
			base.AiTick();
		}

		public override void _Ready()
		{
			base._Ready();
		}
	}
}
