using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class Stag : Critter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 20},
				{TimesOfDay.Noon, 10},
				{TimesOfDay.Dusk, 15},
				{TimesOfDay.Night, 5},
			};
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"deer", "stag", "elk", "red", "grey", "gray", "brown", "fur", "hooves", "hoof", "medium",
				"mid", "small", "big eyes", "large eyes", "white", "horns", "antlers"
			},
			new HashSet<string>
			{
				"roam", "passive", "critter", "prey", "flee", "run", "alone", "packs", "graze", "grass",
				"hide", "docile", "harmless", "bark", "noise", "scream", "sound", "call", "calling",
				"barking", "screaming", "roaming", "grazing", "eating", "running", "fleeing"
			},
			new HashSet<string>
			{
				"random", "forest", "all times", "dusk", "dawn", "morning", "evening", "day", "roam",
				"anywhere", "randomly"
			},
			150,
			220);
		
		public override void PlayPassiveSound()
		{
			AudioStreamPlayer3D player = GetNode<AudioStreamPlayer3D>("Bark");
			int bark = World.Rng.RandiRange(1, 7);
			AudioStreamOggVorbis stream = ResourceLoader.Load<AudioStreamOggVorbis>($"res://Assets/Sound/deer/deer{bark}.ogg");
			player.Stream = stream;
			player.Play();
		}
	}
}
