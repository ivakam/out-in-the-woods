using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class GoatEater : Actor
	{
		[Export] public float AttackCooldown { get; private set; } = 3f;
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"large", "huge", "goat", "antlers", "horn", "green", "skull", "white", "claws", "tall", "enormous",
				"lumbering", "hulking", "beast", "monster", "decay", "stink", "smell", "flies", "rotting", "rot",
				"decaying", "goat", "deer", "stag", "head", "giant", "gigantic", "skin walker", "wendigo"
			},
			new HashSet<string>
			{
				"walk", "stomp", "roam", "laugh", "cackle", "attack", "dangerous", "kill", "close", "swipe", "threat",
				"territory", "hunt", "evil", "malevolent", "malicious", "spirit", "killing", "closely", "swiping",
				"smashing", "smash", "hitting", "hunting", "walking", "stomping", "lumbering", "laughing", "cackling",
				"threatening",
			},
			new HashSet<string>
			{
				"morning", "dawn", "sunrise", "forest", "dusk", "evening", "sunset", "night", "alone", "isolated",
				"random"
			},
			300,
			500);
		
		private bool _attacking = false;
		private Player _player;
		private Area3D _hurtbox;
		
		public void ConnectAnimSignal(string signal, Callable callable)
		{
			Anim.Connect(signal, callable);
		}

		public void PlayAnim(string anim)
		{
			Anim.Play(anim);
		}
		
		public void PlayDarkroomEncounter()
		{
			Anim.Play("Darkroom");
		}
		
		private void PlayCackle()
		{
			GetNode<AudioStreamPlayer3D>("Cackle").Play();
		}

		private void PlayStepSound()
		{
			AudioStreamPlayer3D player = GetNode<AudioStreamPlayer3D>("StepSound");
			int step = World.Rng.RandiRange(1, 6);
			AudioStreamOggVorbis stream = ResourceLoader.Load<AudioStreamOggVorbis>($"res://Assets/Sound/giant/giant{step}.ogg");
			player.Stream = stream;
			player.Play();
		}

		private void Attack()
		{
			_attacking = true;
			GetNode<Timer>("AttackCooldown").Start(AttackCooldown);
			int attack = World.Rng.RandiRange(1, 2);
			Anim.Play($"Attack{attack}");

			AudioStreamPlayer3D laugh = GetNode<AudioStreamPlayer3D>("Laugh");
			AudioStreamOggVorbis stream =
				ResourceLoader.Load<AudioStreamOggVorbis>($"res://Assets/Sound/laugh/laugh{attack}.ogg");
			laugh.Stream = stream;
			laugh.Play();
		}
		
		protected override void AiTick()
		{
			if (!_attacking)
			{
				base.AiTick();
				if (Util.IsNearLocation(GlobalTranslation, _player.GlobalTransform.origin, 5f))
				{
					LookAt(_player.GlobalTransform.origin);
					Attack();
				}
			}
		}

		public override void _PhysicsProcess(double delta)
		{
			if (GetParent() is DarkroomEncounter)
				return;
			base._PhysicsProcess(delta);
		}

		public override void _Ready()
		{
			base._Ready();
			_player = GetTree().Root.GetNode<Player>(World.PlayerPath);
			_hurtbox = GetNode<Area3D>("Hurtbox");
		}
		
		private void OnStep()
		{
			Player p = GetTree().Root.GetNode<Player>(World.PlayerPath);
			float distance = GlobalTransform.origin.DistanceTo(p.GlobalTransform.origin);
			if (distance > 45)
				return;
			float shakeAmount = 1 / (0.05f*distance);
			p.ShakeCamera(shakeAmount);
			PlayStepSound();
		}

		private void OnAttack()
		{
			_hurtbox.Monitoring = true;
		}

		private void DisableHurtbox()
		{
			_hurtbox.Monitoring = false;
			Anim.Play("Idle");
		}
		
		private void OnAttackCooldownTimeout()
		{
			_attacking = false;
		}


		private void OnHurtboxEntered(object body)
		{
			_player.Die();
		}
	}
}
