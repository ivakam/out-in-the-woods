using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class StaticMan : StaticBody3D,IPhotoable
	{
		[Export] public string Title { get; private set; } = "The Static Man";
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"static", "black", "white", "smile", "mouth", "dark", "light", "shining", "faint", "pulsing",
				"pulsating", "man", "humanoid", "person", "human", "silhouette", "shape", "eyes"
			},
			new HashSet<string>
			{
				"static", "noise", "interference", "vision", "glitch", "dangerous", "kill", "harmful", "threat",
				"passive", "still", "stalk", "observe", "disappearing", "killing", "harming", "threatening",
				"stalking", "observing", "passively", "sounding"
			},
			new HashSet<string>
			{
				"night", "dusk", "day", "sunset", "sundown", "evening", "random", "forest", "randomly"
			},
			170,
			200
			);
		
		private Player _player;
		private Timer _killTimer;

		public override void _Ready()
		{
			_player = GetTree().Root.GetNode<Player>(World.PlayerPath);
			_killTimer = GetNode<Timer>("KillTimer");
		}
		
		public override void _PhysicsProcess(double delta)
		{
			Vector3 lookDir = new Vector3
			(
				_player.Position.x,
				GlobalTransform.origin.y,
				_player.Position.z
			);
			LookAt(lookDir);

			float dist = Util.XzDistanceTo(GlobalTransform.origin, _player.GlobalTransform.origin);
			GetTree().Root.GetNode<GUI>("World/GUI").UpdateStaticOverlay(dist);
		}
		
		private void OnKillTimeout()
		{
			if (!Visible)
				return;
			_player.Die();
		}
				
		private void OnKillboxEntered(Node3D player)
		{
			_killTimer.Start(5);
		}

		private void OnKillboxExited(Node3D player)
		{
			_killTimer.Stop();
		}
	}
}




