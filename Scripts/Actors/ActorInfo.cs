using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace outinthewoods
{
    public class ActorInfo
    {
	    private readonly HashSet<string> _appearanceTags;
	    private readonly HashSet<string> _behaviorTags;
	    private readonly HashSet<string> _encounterTags;
	    private readonly float _heightMin;
	    private readonly float _heightMax;
	    
	    public ActorInfo(
		    HashSet<string> appearanceTags,
		    HashSet<string> behaviorTags,
		    HashSet<string> encounterTags,
		    float heightMin,
		    float heightMax
		    )
	    {
		    _appearanceTags = appearanceTags;
		    _behaviorTags = behaviorTags;
		    _encounterTags = encounterTags;
		    _heightMin = heightMin;
		    _heightMax = heightMax;
	    }

	    public static ActorInfo GetActorInfoFromName(string name)
	    {
		    switch (name)
		    {
			    case "Armwalker":
				    return Armwalker.Info;
			    case "Faceless":
				    return Faceless.Info;
			    case "Goat-Eater":
				    return GoatEater.Info;
			    case "Grinner":
				    return Grinner.Info;
			    case "Mimic":
				    return Mimic.Info;
			    case "Orbs":
				    return Orbs.Info;
			    case "Stairs":
				    return Stairs.Info;
			    case "The Static Man":
				    return StaticMan.Info;
			    case "Deer":
				    return Deer.Info;
			    case "Stag":
				    return Stag.Info;
			    case "Fox":
				    return Fox.Info;
		    }

		    return null;
	    }

	    private bool MatchWords(HashSet<string> w1, HashSet<string> w2, int amount)
	    {
		    HashSet<string> res = new HashSet<string>(w1);
		    res.IntersectWith(w2);
		    return res.Count >= amount;
	    }

	    private static HashSet<string> SplitString(string s)
	    {
		    return Regex.Split(s, "\\W+").ToHashSet();
	    }

	    public bool IsFilledOut(string appearance, string behavior,
		    string encounter, string height)
	    {
		    if (!MatchWords(SplitString(appearance), _appearanceTags, 3))
			    return false;
		    if (!MatchWords(SplitString(behavior), _behaviorTags, 3))
				return false;
		    if (!MatchWords(SplitString(encounter), _encounterTags, 3))
				return false;
		    int _height = -1;
		    if (!Int32.TryParse(height, out _height) || _height < _heightMin || _height > _heightMax)
			    return false;
		    return true;
	    }
    }
}