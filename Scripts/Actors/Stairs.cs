using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class Stairs : StaticBody3D,IPhotoable
	{
		
		[Export]
		public float LowPassCutoff
		{
			get
			{
				AudioEffectLowPassFilter lp = (AudioEffectLowPassFilter)AudioServer.GetBusEffect(0, 0);
				return lp.CutoffHz;
			}
			private set
			{
				AudioEffectLowPassFilter lp = (AudioEffectLowPassFilter)AudioServer.GetBusEffect(0, 0);
				lp.CutoffHz = value;
			}
		}
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"stair", "staircase", "stairs", "concrete", "handrail", "cut out", "alone", "gray", "grey",
				"metallic", "plain", "old", "cracked", "dirty"
			},
			new HashSet<string>
			{
				"passive", "sudden", "quiet", "muffle", "muffled", "silence", "silent", "sound", "vision",
				"dark", "black", "pressure", "top", "landing", "orbs", "eerie", "harmless"
			},
			new HashSet<string>
			{
				"clearing", "always", "alone", "no trees", "same place", "persistent", "any time" 
			},
			300,
			500
			);
		
		public string Title { get; } = "Stairs";
		
		private Stairs _stairs;
		
		public void EnableLowPass()
		{
			AudioServer.SetBusEffectEnabled(0, 0, true);
		}
		
		public void DisableLowPass()
		{
			AudioServer.SetBusEffectEnabled(0, 0, false);
		}
		
		private void OnMuffleAreaEntered(Node3D body)
		{
			GetNode<AnimationPlayer>("AnimationPlayer").Play("lowpass_on");
			GetTree().Root.GetNode<AnimationPlayer>("World/GUI/Vignette/AnimationPlayer").Play("vignette_zoom");
		}

		private void OnMuffleAreaExited(Node3D body)
		{
			GetNode<AnimationPlayer>("AnimationPlayer").Play("lowpass_off");
			GetTree().Root.GetNode<AnimationPlayer>("World/GUI/Vignette/AnimationPlayer").PlayBackwards("vignette_zoom");
		}
		
	}
}
