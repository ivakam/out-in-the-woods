using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using Godot;

namespace outinthewoods
{
	/// <summary>
	///     Class for Player controller.
	/// </summary>
	public partial class Player : CharacterBody3D
	{
		[Export] protected string title = "Player";
		
		[Export] protected float acc = 5f;

		[Export] protected float friction = 3f;

		[Export] protected float gravityCoeff = 1f;
		
		[Export] protected float maxFallSpeed = -5f;

		[Export] protected float topSpeed = 3f;

		[Export] protected int maxFilm = 10;

		[Signal] public delegate void PlayerDiedEventHandler();

		public bool Sleeping { get; private set; } = false;
		public bool IsAlive { get; protected set; } = true;
		public CameraRoll CameraRoll { get; } = new CameraRoll();

		private Transform3D _startTransform;
		private Transform3D _cameraDefaultTransform;
		private Vector3 _motion;
		private Bed _bed;
		private RayCast3D _ray;
		private GUI _gui;
		private AnimationPlayer _animation;
		private IInteractable _currInteract = null;
		private bool _cameraMode = false;

		private int RemainingPhotos { get; set; }
		protected List<Pickupable> Inventory { get; } = new List<Pickupable>();


		private void ResetPlayer()
		{
			IsAlive = true;
			GlobalTransform = _startTransform;
			GetNode<Camera3D>("cam/Camera3D").Transform = _cameraDefaultTransform;
			GetNode<Camera3D>("cam/Camera3D").Rotation = Vector3.Zero;
			EmitSignal(nameof(PlayerDiedEventHandler));
			CameraRoll.Clear();
			RestockFilm();
			_animation.Stop();
		}
		
		private void Interact()
		{
			if (!_ray.IsColliding())
				return;

			// Suspicious cast handled by collision layers
			IInteractable interactable = (IInteractable)_ray.GetCollider();
			interactable.OnInteract(this);
		}

		public Vector3 GetSpawnPoint()
		{
			GetNode<Node3D>("SpawnAnchor").RotateY(World.Rng.RandfRange(0, Mathf.Pi * 2));
			return GetNode<Node3D>("SpawnAnchor/SpawnPoint").GlobalTransform.origin;
		}

		protected void HandleEntityPhoto(Image img)
		{
			RayCast3D photoRay = GetNode<RayCast3D>("cam/Camera3D/PhotoRay");
			if (!photoRay.IsColliding())
				return;
			IPhotoable actor = (IPhotoable)photoRay.GetCollider();
			Photo p = Photo.Create(actor.Title, img);
			CameraRoll.Add(p);
		}
		
		private void TakePhoto()
		{
			if (!_cameraMode || RemainingPhotos == 0)
				return;
			RemainingPhotos--;
			_gui.UpdateRemainingFilm(RemainingPhotos, maxFilm);
			_gui.GetNode<AnimationPlayer>("ViewFinder/AnimationPlayer").Play("shutter");
			ViewportTexture tex = GetViewport().GetTexture();
			Rect2i captureRegion = new Rect2i(
				(int)(tex.GetWidth() * 0.242f),
				(int)(tex.GetHeight() * 0.229f),
				(int)(tex.GetWidth() * 0.525f),
				(int)(tex.GetHeight() * 0.6f));
			Image img = GetViewport().GetTexture().GetImage().GetRect(captureRegion);
			HandleEntityPhoto(img);
		}
		
		private void OnGuiHidden()
		{
		}

		private void ToggleCameraMode()
		{
			if (!HasItemWithName("Camera"))
				return;
			_cameraMode = !_cameraMode;
			GetNode<Camera>("cam/Camera3D").ToggleZoom();
			_gui.UpdateRemainingFilm(RemainingPhotos, maxFilm);
			_gui.ToggleCameraOverlay();
		}

		public bool HasItemWithName(string name)
		{
			foreach (Pickupable p in Inventory)
			{
				if (p.Title == name)
					return true;
			}

			return false;
		}

		private void ToggleLight()
		{
			if (!HasItemWithName("Flashlight"))
				return;
			GetNode<AudioStreamPlayer3D>("FlashlightSound").Play();
			SpotLight3D flashlight = GetNode<SpotLight3D>("cam/Camera3D/Flashlight");
			flashlight.Visible = !flashlight.Visible;
		}

		public void RestockFilm()
		{
			GetNode<AudioStreamPlayer3D>("PickupSound").Play();
			RemainingPhotos = maxFilm;
		}

		protected void CheckLookAt()
		{
			if (_cameraMode)
				return;
			RayCast3D lookAt = GetNode<RayCast3D>("cam/Camera3D/LookAtRay");
			if (lookAt.IsColliding())
			{
				LookAtArea l = (LookAtArea)lookAt.GetCollider();
				l.NotifyLookedAt();
			}
		}

		protected void HandleMoveInput()
		{
			_motion = new Vector3
			{
				x = Input.GetActionStrength("move_right") - Input.GetActionStrength("move_left"),
				y = 0f,
				z = Input.GetActionStrength("move_down") - Input.GetActionStrength("move_up")
			};
			_motion = _motion.Rotated(Vector3.Up, Rotation.y);
			if (_motion != Vector3.Zero)
			{
				DirectionalMove(_motion);
			}
			else
			{
				StopAnimate();
			}
		}

		public void ShakeCamera(float amount)
		{
			GetNode<Camera>("cam/Camera3D").AddTrauma(amount);
		}

		public void ViewJournal()
		{
			if (!HasItemWithName("Journal"))
				return;
			_gui.ViewJournal();
		}

		public void Sleep()
		{
			_gui.ShowSleepPrompt();
		}

		public void Die()
		{
			IsAlive = false;
			_animation.PlaybackSpeed = 1;
			_animation.Play("death");
		}

		public void PlayDeathOverlay()
		{
			GetTree().Root.GetNode<ColorRect>("World/GUI/DeathOverlay").Show();
			GetTree().Root.GetNode<AnimationPlayer>("World/GUI/DeathOverlay/AnimationPlayer").Play("fade_in");
		}

		public void MoveToSleepingPosition()
		{
			Sleeping = true;
			GetNode<Node3D>("cam").GlobalTransform = _bed.GetSleepingPosition().GlobalTransform;
			_gui.HideSleepPrompt();
			_gui.HideInteractPrompt();
			_bed.AnimateSleep();
		}

		public void MoveToWakePosition()
		{
			Sleeping = false;
			GetNode<Node3D>("cam").Position = new Vector3(0, 0.8f, 0);
			GetNode<Node3D>("cam").Rotation = Vector3.Zero;
		}

		public void UpdateStatusUI()
		{
			//_hud.UpdateUI();
		}

		public virtual void Pickup(Pickupable p)
		{
			// Maybe play some animation, sound, etc?
			Inventory.Add(p);
			GetNode<AudioStreamPlayer3D>("PickupSound").Play();
		}

		public virtual void PlayStepSound()
		{
			StaticBody3D body = (StaticBody3D)GetNode<RayCast3D>("StepRay").GetCollider();
			string surface = body.PhysicsMaterialOverride?.ResourceName;
			surface ??= "grass";
			AudioStreamPlayer3D player = GetNode<AudioStreamPlayer3D>("StepSound");
			int step = World.Rng.RandiRange(1, 7);
			AudioStreamOggVorbis stream = ResourceLoader.Load<AudioStreamOggVorbis>($"res://Assets/Sound/steps/{surface}{step}.ogg");
			player.Stream = stream;
			if (Input.IsActionPressed("sprint"))
				_animation.PlaybackSpeed = 1.66f;
			else
				_animation.PlaybackSpeed = 1;
			player.Play();
		}

		public virtual void StopAnimate()
		{
			_animation.Stop();
		}
		
		public bool UpdateTranslation()
		{
			if (IsOnFloor())
			{
				var newVel = Velocity.MoveToward(Vector3.Zero, friction);
				Velocity = new Vector3
				{
					x = newVel.x,
					y = Velocity.y,
					z = newVel.z
				};
			}
			else if (Velocity.y > maxFallSpeed)
			{
				var grav = (float) PhysicsServer3D.AreaGetParam(GetViewport().FindWorld3d().Space,
					PhysicsServer3D.AreaParameter.Gravity);
				// TODO: Fetch direction from gravity dir setting
				var gravVec = Vector3.Down * grav * gravityCoeff;
				Velocity += gravVec;
				
			}

			return MoveAndSlide();
		}
		
		protected void DirectionalMove(Vector3 motion)
		{
			_animation.Play("walk");
			if (Util.XzMagnitude(Velocity) < topSpeed)
			{
				motion = motion.Normalized() * acc;
				Velocity += motion;
			}
		}

		public override void _Ready()
		{
			base._Ready();
			_startTransform = GlobalTransform;
			_cameraDefaultTransform = GetNode<Node3D>("cam/Camera3D").Transform;
			_gui = GetNode<GUI>("../../GUI");
			_ray = GetNode<RayCast3D>("cam/Camera3D/InteractRay");
			_animation = GetNode<AnimationPlayer>("AnimationPlayer");
			_bed = GetTree().Root.GetNode<Bed>("World/3DView/NavigationRegion3D/Cabin/Bed_Double");
			RemainingPhotos = maxFilm;
		}

		public override void _PhysicsProcess(double delta)
		{
			if (!Visible || !IsAlive || Sleeping)
				return;
			base._PhysicsProcess(delta);
			if (Input.MouseMode == Input.MouseModeEnum.Visible)
				return;
			if (Input.IsActionJustPressed("journal"))
				ViewJournal();
			if (Input.IsActionJustPressed("light"))
				ToggleLight();
			if (Input.IsActionJustPressed("interact"))
				Interact();
			if (Input.IsActionJustPressed("photo"))
				TakePhoto();
			if (Input.IsActionJustPressed("zoom"))
				ToggleCameraMode();
			if (Input.IsActionJustPressed("sprint"))
				topSpeed += 3;
			else if (Input.IsActionJustReleased("sprint"))
				topSpeed -= 3;
			HandleMoveInput();
			UpdateTranslation();
			
			CheckLookAt();

			// Check if interaction ray is colliding with anything, including a different thing from the previous
			if (_ray.IsColliding() && _currInteract == null ||
				_ray.IsColliding() && _currInteract != (IInteractable)_ray.GetCollider())
			{
				_currInteract = (IInteractable)_ray.GetCollider();
				
				// Broken outline shader, ignore
				/*
				Godot.Collections.Array _meshes = _currInteract.Thing().GetChildren();
				HashSet<MeshInstance3D> meshes = _meshes.OfType<MeshInstance3D>().ToHashSet();
				foreach (MeshInstance3D mesh in meshes)
				{
					mesh.GetActiveMaterial(0).NextPass = _outlineMat;
				*/
				//}
				
				_currInteract.ShowInteractPrompt();
			}
			else if (!_ray.IsColliding() && _currInteract != null && IsInstanceValid(_currInteract.Thing()))
			{
				// Broken outline shader, ignore
				/*
				Godot.Collections.Array _meshes = _currInteract.Thing().GetChildren();
				HashSet<MeshInstance3D> meshes = _meshes.OfType<MeshInstance3D>().ToHashSet();
				foreach (MeshInstance3D mesh in meshes)
				{
					mesh.GetActiveMaterial(0).NextPass = null;
				}
				*/
				_currInteract.HideInteractPrompt();
				_currInteract = null;
			}
		}
	}
}


