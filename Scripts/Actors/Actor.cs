using System.Collections.Generic;
using System.Linq;
using Godot;
using gdcol = Godot.Collections;

namespace outinthewoods
{
	
	/// <summary>
	///     Base class for all characters. Includes player and NPC characters
	/// </summary>
	public abstract partial class Actor : CharacterBody3D,IPhotoable
	{
		[Export] public string Title { get; protected set; } = "ActorName";
		
		[Export] protected float acc = 5f;

		[Export] protected float friction = 3f;

		[Export] protected float gravityCoeff = 1f;
		
		[Export] protected float maxFallSpeed = -50f;

		[Export] protected float topSpeed = 3f;
		
		[Export] public int Hp { get; protected set; } = 100;
		
		[Export] public int MaxHp { get; protected set; } = 100;

		public OverheadManager OverheadManager { get; protected set; }

		protected NavigationAgent3D Nav { get; set; }
		protected AnimationPlayer Anim { get; set; }
		protected double CDelta { get; set; }
		protected Vector3 startPos;

		public Vector3 GlobalTranslation => GlobalTransform.origin;

		public virtual Node3D Thing()
		{
			return this;
		}

		public virtual string GetDebugStr()
		{
			var res = "";
			res += $"HP: {Hp}\n";
			res += $"Velocity: {Velocity}\n";
			return res;
		}

		public bool UpdateTranslation()
		{
			if (IsOnFloor())
			{
				var newVel = Velocity.MoveToward(Vector3.Zero, friction);
				Velocity = new Vector3
				{
					x = newVel.x,
					y = Velocity.y,
					z = newVel.z
				};
			}
			else if (Velocity.y > maxFallSpeed)
			{
				var grav = (float) PhysicsServer3D.AreaGetParam(GetViewport().FindWorld3d().Space,
					PhysicsServer3D.AreaParameter.Gravity);
				// TODO: Fetch direction from gravity dir setting
				var gravVec = Vector3.Down * grav * gravityCoeff;
				Velocity += gravVec;
				
			}

			Nav.SetVelocity(Velocity);
			return false;
		}

		public void PositionalMove(Vector3 dest)
		{
			Anim.Play("WalkCycle");
			Vector3 lookDir = new Vector3(dest.x, GlobalTranslation.y, dest.z);
			if (GlobalTranslation != lookDir)
				LookAt(lookDir, Vector3.Up);
			Vector3 force = dest - GlobalTranslation;
			force.y = 0;
			if (Util.XzMagnitude(Velocity) < topSpeed)
			{
				// GD.Print($"Moving to {dest} from {GlobalTranslation}. {Util.XzDistanceTo(dest, GlobalTranslation)} remaining");
				force = force.Normalized() * acc;
				Velocity += force;
			}
		}
		
		public Vector3 GetWanderTarget()
		{
			var newVec = new Vector3
			{
				x = 5f,
				y = 0,
				z = 0
			};
			newVec *= World.Rng.Randf();
			newVec = newVec.Rotated(Vector3.Up, World.Rng.RandfRange(0, 2 * Mathf.Pi));
			var idleLoc = startPos + newVec;
			return idleLoc;
		}

		protected void Idle()
		{
			Anim.Play("Idle");
			GetNode<Timer>("IdleTimer").Start(World.Rng.Randf() * 5);
		}

		protected virtual void OnIdleTimeout()
		{
			do
			{
				Nav.SetTargetLocation(GetWanderTarget());
				Nav.SetTargetLocation(Nav.GetFinalLocation());
			} while (!Nav.IsTargetReachable());
		}
		
		private void OnSafeVelocity(Vector3 safeVelocity)
		{
			Velocity = safeVelocity;
			MoveAndSlide();
		}

		protected virtual void AiTick()
		{
			Timer idleTimer = GetNode<Timer>("IdleTimer");
			
			if (idleTimer.TimeLeft > 0)
				return;
			if (Nav.GetTargetLocation() == Vector3.Zero || Nav.IsTargetReached())
				Idle();
			else
				PositionalMove(Nav.GetNextLocation());
		}

		public override void _Ready()
		{
			startPos = GlobalTranslation;
			Nav = GetNode<NavigationAgent3D>("NavigationAgent3D");
			Anim = GetNode<AnimationPlayer>("AnimationPlayer");
		}

		public override void _PhysicsProcess(double delta)
		{
			CDelta = delta;
			if (!Visible)
				return;
			AiTick();
			UpdateTranslation();
		}
	}
}
