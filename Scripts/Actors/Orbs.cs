using Godot;
using System;
using System.Collections.Generic;

namespace outinthewoods
{
	public partial class Orbs : StaticBody3D,IPhotoable
	{
		[Export] public string Title { get; private set; } = "Orbs";
		
		public static ActorInfo Info { get; } = new ActorInfo(
			new HashSet<string>
			{
				"yellow", "shine", "light", "shiny", "shimmering", "ethereal", "float", "flying", "hovering",
				"hover", "dreamy", "ghostly", "orbs", "balls", "spheres", "spherical", "round"
			},
			new HashSet<string>
			{
				"lead", "lost", "lure", "sound", "chimes", "trick", "stairs", "victim", "prey", "mysterious",
				"serene", "calm", "teleport", "phase", "disappear", "vanish", "reappear", "luring", "leading",
				"chiming", "calming", "teleporting", "phasing", "disappearing", "vanishing", "victims", "tricking"
			},
			new HashSet<string>
			{
				"dawn", "morning", "sunrise", "noon", "day", "daylight", "dusk", "evening", "sunset", "away",
				"alone", "several"
			},
			20,
			80
			);
		
		private Marker3D _goal;

		protected void OnFadeIn()
		{
		}

		protected void OnFadeOut()
		{
			if (Util.IsNearLocation(GlobalTransform.origin, _goal.GlobalTransform.origin, 5))
			{
				QueueFree();
				return;
			}
			Hide();
			Vector3 nextPos = GlobalTransform.origin.MoveToward(
				_goal.GlobalTransform.origin,
				25);
			GlobalTranslate(nextPos - GlobalTransform.origin);
			Show();
			GetNode<AnimationPlayer>("Fade").Play("fade_in");
		}
		
		private void OnFadeRangeEntered(object body)
		{
			GetNode<AnimationPlayer>("Fade").Play("fade_out");
		}

		public override void _Ready()
		{
			GetNode<AnimationPlayer>("Float").Play("float");
			_goal = (Marker3D)GetTree().GetFirstNodeInGroup("orb_goal");
		}
	}
}
