using System;
using System.Collections.Generic;
using System.Linq;
using Godot;

namespace outinthewoods
{
	public class LotteryEntry
	{
		private readonly PackedScene _scene;
		public Godot.Collections.Dictionary<TimesOfDay, int> Tickets { get; private set; }
		
		public LotteryEntry(PackedScene scene, Godot.Collections.Dictionary<TimesOfDay,int> tickets)
		{
			_scene = scene;
			Tickets = tickets;
		}

		public Node3D InstantiateScene() => _scene.Instantiate<Node3D>();
	}
	
    public static class LotteryMachine
    {
        public static List<Node3D> PickWinners(HashSet<LotteryEntry> entries, TimesOfDay time, int repeats)
        {
            List<LotteryEntry> tickets = new List<LotteryEntry>();
            List<Node3D> winners = new List<Node3D>();
            
            foreach (LotteryEntry entry in entries)
            {
                for (int i = 0; i < entry.Tickets[time]; i++)
                {
                    tickets.Add(entry);
                }
            }

            for (int i = 0; i < repeats; i++)
            {
                // Add a duplicate of the given instantiated lottery entry to winners
                winners.Add(tickets.ElementAt(World.Rng.RandiRange(0, tickets.Count - 1)).InstantiateScene());
            }

            return winners;
        }
    }
}