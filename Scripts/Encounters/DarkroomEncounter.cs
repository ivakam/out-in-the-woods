using Godot;

namespace outinthewoods
{
	public partial class DarkroomEncounter : Encounter
	{
		private GoatEater _goatEater;
		private AnimationPlayer _cabinAnim;
		private StaticBody3D _cabin;

		public override void _Ready()
		{
			base._Ready();
			encounterArea.Monitoring = false;
			_goatEater = GetNode<GoatEater>("Goat_Eater");

			_cabin = GetTree().Root
				.GetNode<StaticBody3D>("World/3DView/NavigationRegion3D/Cabin/Cabin");
			_cabinAnim = _cabin.GetNode<AnimationPlayer>("house3/AnimationPlayer");
			Door door = _cabin.GetNode<Door>("house3/DarkroomDoor");
			
			door.Lock();
			StartEncounter();
		}
		
		private void StartEncounter()
		{
			GetNode<AnimationPlayer>("Slide").Play("slide_in");
			_goatEater.PlayAnim("WalkCycle");
		}

		private void OnSlideInFinished()
		{
			_cabinAnim.Connect("animation_finished", new Callable(this, nameof(OnWindowsOpen)));
			_cabinAnim.Play("open_windows");
		}

		private void OnWindowsOpen(StringName animName)
		{
			//TODO: Disconnect window signal
			if (animName != "open_windows")
				return;
			_goatEater.ConnectAnimSignal("animation_finished", new Callable(this, nameof(OnDarkroomFinished)));
			_goatEater.PlayDarkroomEncounter();
		}

		private void OnDarkroomFinished(StringName animName)
		{
			_cabinAnim.Play("close_windows");
			GetNode<AnimationPlayer>("Slide").Play("slide_out");
			_goatEater.PlayAnim("WalkCycle");
		}

		private void OnSlideOutFinished()
		{
			Door door = _cabin.GetNode<Door>("house3/DarkroomDoor");
			door.Unlock();
			
			EmitSignal("EncounterFinished", this);
		}
	}
}


