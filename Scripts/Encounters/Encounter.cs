using Godot;
using System;

namespace outinthewoods
{
	public abstract partial class Encounter : Node3D
	{
		[Signal] public delegate void EncounterFinishedEventHandler(Encounter encounter);
		[Export] public string EncounterName { get; protected set; }
		
		private readonly PackedScene _encounterAreaScene = ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/EncounterArea.tscn");
		protected Area3D encounterArea;

		protected virtual void OnAreaExited(Node3D body)
		{
			EmitSignal(nameof(EncounterFinishedEventHandler), this);
		}
		
		public override void _PhysicsProcess(double delta)
		{
			base._PhysicsProcess(delta);
			// 1 child since the encounter area is there
			if (GetChildCount() == 1)
			{
				EmitSignal(nameof(EncounterFinishedEventHandler), this);
			}
		}

		public override void _Ready()
		{
			base._Ready();
			Connect("EncounterFinished",
				new Callable(GetTree().Root.GetNode<World>("World"),
				nameof(World.OnEncounterFinished)));
			encounterArea = _encounterAreaScene.Instantiate<Area3D>();
			encounterArea.Connect("body_exited", new Callable(this, nameof(OnAreaExited)));
			encounterArea.Monitoring = true;
			AddChild(encounterArea);
		}
	}
}
