using Godot;
using System;

namespace outinthewoods
{
	public partial class MimicEncounter : Encounter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 5},
				{TimesOfDay.Noon, 3},
				{TimesOfDay.Dusk, 25},
				{TimesOfDay.Night, 10},
			};
	}
}
