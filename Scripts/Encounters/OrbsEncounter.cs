using Godot;
using System;

namespace outinthewoods
{
	public partial class OrbsEncounter : Encounter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 20},
				{TimesOfDay.Noon, 15},
				{TimesOfDay.Dusk, 15},
				{TimesOfDay.Night, 0},
			};
		
		private Orbs _orbs;

		protected override void OnAreaExited(Node3D body)
		{
			return;
		}

		public override void _Ready()
		{
			base._Ready();
			_orbs = GetNode<Orbs>("Orbs");
			encounterArea.Monitoring = false;
		}
	}
}
