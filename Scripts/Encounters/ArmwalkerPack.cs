using Godot;
using System;
using System.Collections.Generic;
using outinthewoods;

namespace outinthewoods
{
	public partial class ArmwalkerPack : Encounter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 30},
				{TimesOfDay.Noon, 5},
				{TimesOfDay.Dusk, 5},
				{TimesOfDay.Night, 0},
			};

		public HashSet<Node3D> Trees { get; }= new HashSet<Node3D>();

		public override void _Ready()
		{
			base._Ready();
		}

		private void OnTreeAreaEntered(Node3D body)
		{
			if (body.IsInGroup("tree"))
				Trees.Add(body);
		}
	}
}


