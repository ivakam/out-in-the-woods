using Godot;
using System;

namespace outinthewoods
{
	public partial class GrinnerPack : Encounter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 5},
				{TimesOfDay.Noon, 5},
				{TimesOfDay.Dusk, 10},
				{TimesOfDay.Night, 30},
			};
	}
}
