using Godot;
using System;

namespace outinthewoods
{
    public partial class StairsEncounter : Encounter
    {
        public override void _Ready()
        {
            base._Ready();
            encounterArea.Monitoring = false;
        }
    }
}
