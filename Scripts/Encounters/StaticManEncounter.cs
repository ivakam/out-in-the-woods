using Godot;

namespace outinthewoods
{
	public partial class StaticManEncounter : Encounter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 0},
				{TimesOfDay.Noon, 1},
				{TimesOfDay.Dusk, 15},
				{TimesOfDay.Night, 30},
			};
		
		private StaticMan _staticMan;

		public override void _Ready()
		{
			base._Ready();
			_staticMan = GetNode<StaticMan>("StaticMan");
			World w = GetTree().Root.GetNode<World>("World");
			if (!w.EncounteredStaticMan)
			{
				_staticMan.Hide();
				w.EncounteredStaticMan = true;
			}
		}
	}
}
