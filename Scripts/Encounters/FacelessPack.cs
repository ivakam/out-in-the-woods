using Godot;
using System;

namespace outinthewoods
{
	public partial class FacelessPack : Encounter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 0},
				{TimesOfDay.Noon, 5},
				{TimesOfDay.Dusk, 35},
				{TimesOfDay.Night, 10},
			};
	}
}
