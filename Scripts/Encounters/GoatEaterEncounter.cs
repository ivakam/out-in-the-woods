using Godot;
using System;

namespace outinthewoods
{
	public partial class GoatEaterEncounter : Encounter
	{
		public static Godot.Collections.Dictionary<TimesOfDay,int> LotteryTickets { get; } =
			new Godot.Collections.Dictionary<TimesOfDay,int>
			{
				{TimesOfDay.Dawn, 10},
				{TimesOfDay.Noon, 0},
				{TimesOfDay.Dusk, 30},
				{TimesOfDay.Night, 10},
			};
	}
}
