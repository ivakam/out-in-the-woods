using Godot;
using System;

namespace outinthewoods
{
	public partial class Pond : Node3D
	{
		private readonly PackedScene _ladyEncounter =
			ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/LadyLichenEncounter.tscn");

		private LadyLichenEncounter _lady;
		
		private void OnPlayerEntered(Node3D player)
		{
			if (GetTree().Root.GetNode<World>("World").TimeOfDay == TimesOfDay.Dawn)
			{
				_lady = _ladyEncounter.Instantiate<LadyLichenEncounter>();
				AddChild(_lady);
			}
		}

		private void OnPlayerExited(Node3D player)
		{
			if (_lady != null)
			{
				_lady.QueueFree();
				_lady = null;
			}
		}
	}
}
