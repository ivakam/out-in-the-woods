using System.Collections.Generic;
using Godot;

namespace outinthewoods
{
    
    public class CameraRoll
    {
        public static readonly int Capacity = 10;

        private readonly Queue<Photo> _images = new Queue<Photo>();
        
        public Queue<Photo> GetImages()
        {
            Queue<Photo> res = new Queue<Photo>();
            foreach (Photo p in _images)
            {
                res.Enqueue(Photo.Create(p));
            }

            return res;
        }

        public Dictionary<string, List<Photo>> ToDict()
        {
            Dictionary<string, List<Photo>> res = new Dictionary<string, List<Photo>>();
            foreach (Photo p in _images)
            {
                if (!res.ContainsKey(p.Entity))
                    res[p.Entity] = new List<Photo>();
                res[p.Entity].Add(Photo.Create(p));
            }

            return res;
        }

        public void Add(Photo photo)
        {
            if (_images.Count >= Capacity)
            {
                _images.Dequeue();
            }
            _images.Enqueue(photo);
        }

        public void Clear()
        {
            _images.Clear();
        }
    }
}