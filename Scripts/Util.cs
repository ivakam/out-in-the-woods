using System;
using System.Collections.Generic;
using System.Linq;
using Godot;
using Col = Godot.Collections;
using Object = Godot.Object;

namespace outinthewoods
{
    /// <summary>
    ///     Static utility functions and enums
    /// </summary>
    public static class Util
    {
        public enum Orientations
        {
            N,
            E,
            S,
            W,
            NE,
            NW,
            SE,
            SW
        }

        public static Col.Dictionary<Orientations, Vector3> DIRECTIONS = new Col.Dictionary<Orientations, Vector3>
        {
            {Orientations.N, Vector3.Forward},
            {Orientations.E, Vector3.Right},
            {Orientations.S, Vector3.Back},
            {Orientations.W, Vector3.Left},
            {Orientations.NE, new Vector3(1 / Mathf.Sqrt(2), 0, -1 / Mathf.Sqrt(2))},
            {Orientations.NW, new Vector3(-1 / Mathf.Sqrt(2), 0, -1 / Mathf.Sqrt(2))},
            {Orientations.SE, new Vector3(1 / Mathf.Sqrt(2), 0, 1 / Mathf.Sqrt(2))},
            {Orientations.SW, new Vector3(-1 / Mathf.Sqrt(2), 0, 1 / Mathf.Sqrt(2))}
        };

        public static Col.Dictionary<Orientations, int> ROTATIONS = new Col.Dictionary<Orientations, int>
        {
            {Orientations.N, 0},
            {Orientations.E, -90},
            {Orientations.S, 180},
            {Orientations.W, 90},
            {Orientations.NE, -45},
            {Orientations.NW, 45},
            {Orientations.SE, -135},
            {Orientations.SW, 135}
        };
        
        public static bool IsNearLocation(Vector3 a, Vector3 b, float tolerance)
        {
            return XzDistanceTo(a, b) < tolerance;
        }
        
        public static Node3D GetClosest(Vector3 to, HashSet<Node3D> things){
            Node3D closest = null;
            var closestDist = float.PositiveInfinity;
            foreach (var s in things)
                if (Object.IsInstanceValid(s))
                {
                    var dist = to.DistanceTo(s.GlobalTransform.origin);
                    if (dist < closestDist)
                    {
                        closestDist = dist;
                        closest = s;
                    }
                }
            return closest;
        }
        
        public static Node3D GetClosest(Vector3 to, List<Node3D> things)
        {
            return GetClosest(to, things.ToHashSet());
        }

        public static Node3D GetClosest(Vector3 to, Col.Array things)
        {
            return GetClosest(to, things.Cast<Node3D>().ToHashSet());
        }

        public static float XzMagnitude(Vector3 vec)
        {
            return new Vector2(vec.x, vec.z).Length();
        }

        public static float XzDistanceTo(Vector3 from, Vector3 to)
        {
            return new Vector2(from.x, from.z).DistanceTo(new Vector2(to.x, to.z));
        }

        public static Vector3 XzMoveToward(Vector3 from, Vector3 to, float delta)
        {
            Vector3 xzFrom = new Vector3(from.x, 0, from.z);
            Vector3 xzTo = new Vector3(to.x, 0, to.z);
            Vector3 res = xzFrom.MoveToward(xzTo, delta);

            return new Vector3(res.x, from.y, res.z);
        }

        public static Orientations GetOrientation(Vector3 vec)
        {
            var proj = new Vector2(vec.x, vec.z);
            var dir = new Tuple<float, float>(Mathf.Cos(proj.Angle()), Mathf.Sin(proj.Angle()));
            if (dir.Item2 >= Mathf.Sin(3 * Mathf.Pi / 8))
                return Orientations.S;
            if (dir.Item2 <= -Mathf.Sin(3 * Mathf.Pi / 8))
                return Orientations.N;
            if (dir.Item1 >= Mathf.Cos(Mathf.Pi / 8))
                return Orientations.E;
            if (dir.Item1 <= Mathf.Cos(7 * Mathf.Pi / 8))
                return Orientations.W;
            if (dir.Item1 >= Mathf.Sin(Mathf.Pi / 8) && dir.Item2 >= Mathf.Cos(3 * Mathf.Pi / 8))
                return Orientations.SE;
            if (dir.Item1 >= Mathf.Sin(Mathf.Pi / 8) && dir.Item2 <= Mathf.Cos(5 * Mathf.Pi / 8))
                return Orientations.NE;
            if (dir.Item1 <= -Mathf.Sin(Mathf.Pi / 8) && dir.Item2 >= Mathf.Cos(3 * Mathf.Pi / 8))
                return Orientations.SW;
            if (dir.Item1 <= -Mathf.Sin(Mathf.Pi / 8) && dir.Item2 <= Mathf.Cos(5 * Mathf.Pi / 8))
                return Orientations.NW;
            return Orientations.N;
        }

        public static World3D GetWorld(Node n) => n.GetViewport().FindWorld3d();
    }
}