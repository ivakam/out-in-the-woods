using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace outinthewoods
{
	public enum TimesOfDay
	{
		Dawn,
		Noon,
		Dusk,
		Night
	}
	
	public partial class World : Node3D
	{
		public static readonly RandomNumberGenerator Rng = new RandomNumberGenerator();
		public static readonly string PlayerPath = "World/3DView/Player";
		
		private Timer _encounterTimer;
		private Encounter _currentEncounter;
		private Player _player;
		
		/*
		 * Encounter variables, might get refactored somewhere
		 */
		public bool EncounteredStaticMan { get; set; } = false;
		
		// Whether or not the player is in the safe zone near the cabin
		private bool _inSafeZone = false;
		
		// Keeps track of what the coming time of day is for the sleep animation callback
		private TimesOfDay _comingTime;

		public TimesOfDay TimeOfDay { get; private set; } = TimesOfDay.Noon;

		private List<Marker3D> _spawnPoints;
		
		private static readonly HashSet<LotteryEntry> Critters = new HashSet<LotteryEntry>
		{
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Actors/Deer.tscn"), Deer.LotteryTickets),
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Actors/Stag.tscn"), Stag.LotteryTickets),
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Actors/Fox.tscn"), Fox.LotteryTickets),
		};

		private static readonly HashSet<LotteryEntry> Encounters = new HashSet<LotteryEntry>
		{
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/ArmwalkerPack.tscn"), ArmwalkerPack.LotteryTickets),
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/FacelessPack.tscn"), FacelessPack.LotteryTickets),
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/GoatEaterEncounter.tscn"), GoatEaterEncounter.LotteryTickets),
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/StaticManEncounter.tscn"), StaticManEncounter.LotteryTickets),
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/OrbsEncounter.tscn"), OrbsEncounter.LotteryTickets),
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/MimicEncounter.tscn"), MimicEncounter.LotteryTickets),
			new LotteryEntry(ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/GrinnerPack.tscn"), GrinnerPack.LotteryTickets),
		};

		public bool EncounterInProgress => _currentEncounter == null;
		
		private void KillLights()
		{
			foreach (DirectionalLight3D l in GetTree().GetNodesInGroup("light"))
			{
				l.Visible = false;
			}
		}

		private void KillSound()
		{
			foreach (AudioStreamPlayer a in GetTree().GetNodesInGroup("ambience"))
			{
				a.Stop();
			}
		}

		private void SetCurrentEncounter(Encounter encounter, Vector3 position)
		{
			ResetEncounter();
			_currentEncounter = encounter;
			_currentEncounter.Position = position;
			GetNode<Node3D>("3DView/NavigationRegion3D").AddChild(encounter);
		}

		private void ResetEncounter(Encounter e = null)
		{
			e?.QueueFree();
			_currentEncounter?.QueueFree();
			_currentEncounter = null;
			_encounterTimer.Start(10);
		}

		private void PlayAmbiance(TimesOfDay time)
		{
			KillSound();
			switch (time)
			{
				case TimesOfDay.Noon:
					GetNode<AudioStreamPlayer>("AmbienceNoon").Play();
					break;
				case TimesOfDay.Dawn:
					GetNode<AudioStreamPlayer>("AmbienceDawn").Play();
					break;
				case TimesOfDay.Dusk:
					GetNode<AudioStreamPlayer>("AmbienceDusk").Play();
					break;
				case TimesOfDay.Night:
					GetNode<AudioStreamPlayer>("AmbienceNight").Play();
					break;
			}
		}
		
		protected void SpawnCritters()
		{
			float critterSpawnRate = 0.2f;
			
			foreach (Node n in GetTree().GetNodesInGroup("critter"))
			{
				n.QueueFree();
			}
			// Populate every spawn point with a random critter
			List<Critter> critters = LotteryMachine.PickWinners(Critters, TimeOfDay, (int) (_spawnPoints.Count * critterSpawnRate))
				.Cast<Critter>()
				.ToList();

			foreach (Marker3D point in _spawnPoints.OrderBy(x => Rng.Randf()))
			{
				if (critters.Count == 0)
					break;
				Critter c = critters.First();
				c.Position = point.GlobalTransform.origin;
				GetNode<Node3D>("3DView/NavigationRegion3D").AddChild(c);
				critters.Remove(c);
			}
		}

		private SpawnPoint GetBestSpawnPoint()
		{
			return (SpawnPoint)Util.GetClosest(_player.GetSpawnPoint(), _spawnPoints.Cast<Node3D>().ToList());
		}
		

		// Places spawn points in concentric circles around the world origin
		private void PlaceSpawnPoints()
		{
			PackedScene spawnPoint = ResourceLoader.Load<PackedScene>("res://Scenes/SpawnPoint.tscn");
			Node3D boundaryNorth = GetNode<Node3D>("3DView/MapBoundary/North");
			Node3D spawnPointsNode = GetNode<Node3D>("3DView/NavigationRegion3D/SpawnPoints");
			int boundaryMargin = 20;

			float boundary = boundaryNorth.Position.z + boundaryMargin;
			
			Vector3 spawnPos = new Vector3(boundary, 20, boundary);
			float stepSize = 15f;
			int stepAmount = 2 * (int)Math.Ceiling(Math.Abs(boundary / stepSize));

			// Move in a grid placing spawnpoints
			for (int i = 0; i < stepAmount; i++)
			{
				for (int j = 0; j < stepAmount; j++)
				{
					SpawnPoint p = spawnPoint.Instantiate<SpawnPoint>();
					p.Position = new Vector3(spawnPos.x, spawnPos.y, spawnPos.z);
					spawnPointsNode.AddChild(p);

					spawnPos.z += stepSize;
				}
				spawnPos.x += stepSize;
				spawnPos.z = boundary;
			}
		}

		private void SetTimeOfDay(TimesOfDay time)
		{
			TimeOfDay = time;
			KillLights();
			if (time == TimesOfDay.Night)
			{
				if (Rng.Randf() > 0.7)
				{
					GetNode<DirectionalLight3D>("3DView/Moon").Visible = true;
				}
			}
			else
			{
				GetNode<DirectionalLight3D>($"3DView/{time.ToString()}").Visible = true;
			}
			PlayAmbiance(time);
			SpawnCritters();
		}
		
		/*
		 * Built-in overrides
		 */

		public override void _Ready()
		{
			GD.Print("t1");
			PlaceSpawnPoints();
			_player = GetNode<Player>("3DView/Player");
			_encounterTimer = GetNode<Timer>("EncounterTimer");
			_spawnPoints = GetNode("3DView/NavigationRegion3D/SpawnPoints")
				.GetChildren()
				.Cast<Marker3D>()
				.ToList();
			SetTimeOfDay(TimesOfDay.Noon);
			
			_encounterTimer.Start(10);
		}
		
		public override void _PhysicsProcess(double delta)
		{
			GetNode<GUI>("GUI").UpdateDebug(_encounterTimer.TimeLeft, _currentEncounter?.EncounterName);
		}
		
		public override void _Input(InputEvent @event)
		{
			GetNode<SubViewport>("3DView").PushInput(@event);
		}
		
		/*
		 * Signal handlers
		 */
		
		private void OnEncounterTimeout()
		{
			if (_currentEncounter != null)
				return;
			if (!_inSafeZone && Rng.Randf() > 0)
			{
				SpawnPoint point = GetBestSpawnPoint();
				Encounter encounter = (Encounter)LotteryMachine.PickWinners(Encounters, TimeOfDay, 1)
					.ToList().First();
				SetCurrentEncounter(encounter, point.GlobalTransform.origin);
			}
			else
			{
				_currentEncounter = null;
				_encounterTimer.Start(10);
			}
		}
		
		private void OnPlayerDied()
		{
			GetNode<GUI>("GUI").ResetOverlays();
			SetTimeOfDay((TimesOfDay)(((int)TimeOfDay + 1) % 4));
			ResetEncounter();
		}
		
		private void OnSafeZoneEntered(Node3D body)
		{
			_inSafeZone = true;
		}

		private void OnSafeZoneExited(Node3D body)
		{
			_inSafeZone = false;
		}
		
		public void OnEncounterFinished(Encounter e)
		{
			ResetEncounter(e);
		}
		
		private void OnDawnButtonPressed()
		{
			_comingTime = TimesOfDay.Dawn;
			ResetEncounter();
			_player.MoveToSleepingPosition();
		}

		private void OnNoonButtonPressed()
		{
			_comingTime = TimesOfDay.Noon;
			ResetEncounter();
			_player.MoveToSleepingPosition();
		}

		private void OnDuskButtonPressed()
		{
			_comingTime = TimesOfDay.Dusk;
			ResetEncounter();
			_player.MoveToSleepingPosition();
		}

		private void OnNightButtonPressed()
		{
			_comingTime = TimesOfDay.Night;
			ResetEncounter();
			_player.MoveToSleepingPosition();
		}
		
		private void OnResumePressed()
		{
			GetNode<GUI>("GUI").HidePauseMenu();
		}

		private void OnExitPressed()
		{
			GetTree().Quit();
		}

		private void OnSleepFinished()
		{
			SetTimeOfDay(_comingTime);
			_player.MoveToWakePosition();
		}
		private void OnDarkroomEncounter()
		{
			PackedScene enc = ResourceLoader.Load<PackedScene>("res://Scenes/Encounters/DarkroomEncounter.tscn");
			DarkroomEncounter drk = enc.Instantiate<DarkroomEncounter>();
			SetCurrentEncounter(drk, GlobalTransform.origin);
		}
	}
}
