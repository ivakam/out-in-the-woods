using Godot;

namespace outinthewoods
{
    public partial class Vats : StaticBody3D,IInteractable
    {
        [Signal]
        public delegate void DarkroomEncounterEventHandler();

        private bool _encountered;
        
        public Node3D Thing()
        {
            return this;
        }

        public OverheadManager OverheadManager { get; }
        
        public void ShowInteractPrompt()
        {
			GetTree().Root.GetNode<GUI>("World/GUI").ShowInteractPrompt("Manage photos [E]");
        }

        public void HideInteractPrompt()
        {
			GetTree().Root.GetNode<GUI>("World/GUI").HideInteractPrompt();
        }
        public void OnInteract(Player player)
        {
            TimesOfDay time = GetTree().Root.GetNode<World>("World").TimeOfDay;
            // Encounter only fires if it hasn't before, it is night/dusk, and a 10% chance roll
            if (!_encountered && 
                (time == TimesOfDay.Night || time == TimesOfDay.Dusk) 
                && World.Rng.Randf() > 0.9f)
            {
                _encountered = true;
                EmitSignal(nameof(DarkroomEncounterEventHandler));
            }
            else
            {
                GetTree().Root.GetNode<GUI>("World/GUI").ShowPhotoManager(player);
            }
        }
    }
}
