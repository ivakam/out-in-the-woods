using Godot;
using System;

namespace outinthewoods
{
	public partial class Pickupable : StaticBody3D,IInteractable
	{
		//[Export] protected string InteractText { get; set; }
		[Export] public string Title { get; protected set; }
		
		public OverheadManager OverheadManager { get; private set; }
		
		public Node3D Thing()
		{
			return this;
		}

		public void ShowInteractPrompt()
		{
			//OverheadManager.ShowInteractPrompt();
			GetTree().Root.GetNode<GUI>("World/GUI").ShowInteractPrompt("Pick up [E]");
		}

		public void HideInteractPrompt()
		{
			//OverheadManager.HideInteractPrompt();
			GetTree().Root.GetNode<GUI>("World/GUI").HideInteractPrompt();
		}

		public void OnInteract(Player player)
		{
			player.Pickup(this);
			HideInteractPrompt();
			QueueFree();
		}

		public override void _Ready()
		{
			base._Ready();
			OverheadManager = new OverheadManager(this, "Pick up [E]");
		}
	}
}
