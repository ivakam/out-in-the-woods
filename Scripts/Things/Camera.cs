using System;
using Godot;

namespace outinthewoods
{
	/// <summary>
	///     3rd person camera for player viewport
	/// </summary>
	public partial class Camera : Camera3D
	{
		private static readonly float _CamVMax = Mathf.Pi / 2;
		private static readonly float _CamVMin = -Mathf.Pi / 2;

		[Export] private float _decay = 1.2f;
		[Export] private Vector2 _maxOffset = new Vector2(0.1f, 0.075f);

		private float _camrootH;
		private float _camrootV;
		private readonly float hSens = 0.0017f;
		private readonly float hSensLow = 0.001f;
		private readonly float vSens = 0.0017f;
		private readonly float vSensLow = 0.001f;
		private bool _zoomed = false;
		private float _trauma = 0.0f;
		private float _traumaPower = 2;
		private float _noiseY = 0;
		private readonly FastNoiseLite _noise = new FastNoiseLite();

		public override void _Ready()
		{
			//Hide mouse
			Input.MouseMode = Input.MouseModeEnum.Captured;
			_noise.Seed = (int)World.Rng.Randi();
			_noise.NoiseType = FastNoiseLite.NoiseTypeEnum.SimplexSmooth;
			_noise.FractalOctaves = 2;
		}

		public void ToggleZoom()
		{
			AnimationPlayer dolly = GetNode<AnimationPlayer>("ZoomDolly");
			if (_zoomed)
				dolly.Play("ZoomOut");
			else
				dolly.Play("ZoomIn");
			_zoomed = !_zoomed;
		}

		public void AddTrauma(float amount)
		{
			_trauma = Mathf.Min(_trauma + amount, 1.0f);
		}

		private void Shake()
		{
			float amount = Mathf.Pow(_trauma, _traumaPower);
			_noiseY += 1;
			HOffset = _maxOffset.x * amount * _noise.GetNoise2d(_noise.Seed * 2, _noiseY);
			VOffset = _maxOffset.y * amount * _noise.GetNoise2d(_noise.Seed * 3, _noiseY);
		}

		public override void _PhysicsProcess(double delta)
		{
			if (_trauma > 0)
			{
				_trauma = Mathf.Max((float)(_trauma - _decay * delta), 0.0f);
				Shake();
			}
		}

		public override void _Input(InputEvent @event)
		{
			Player player = (Player) GetParent().GetParent();
			if (Input.MouseMode == Input.MouseModeEnum.Visible || !player.IsAlive)
				return;
			if (@event is InputEventMouseMotion motion)
			{
				_camrootH = (_zoomed) ? -motion.Relative.x * hSensLow : -motion.Relative.x * hSens;
				_camrootV = (_zoomed) ? -motion.Relative.y * vSensLow : -motion.Relative.y * vSens;
				if (player.Sleeping)
					return;
				player.RotateY(_camrootH);

				var vRot = Mathf.Clamp(_camrootV, _CamVMin, _CamVMax);
				Node3D cam = player.GetNode<Node3D>("cam");
				cam.RotateX(vRot);
				cam.Rotation = new Vector3(
					Mathf.Clamp(cam.Rotation.x + _camrootV, _CamVMin, _CamVMax),
					cam.Rotation.y,
					cam.Rotation.z);
			}
		}
	}
}
