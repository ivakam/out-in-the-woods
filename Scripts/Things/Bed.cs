using Godot;
using System;

namespace outinthewoods
{
	public partial class Bed : StaticBody3D,IInteractable
	{
		public OverheadManager OverheadManager { get; protected set; }
		public string Title { get; } = "Bed";
		
		public Node3D Thing()
		{
			return this;
		}

		public void AnimateSleep()
		{
			AnimationPlayer anim = GetTree().Root.GetNode<AnimationPlayer>("World/GUI/SleepOverlay/AnimationPlayer");
			anim.Play("fade_in");
		}

		public Marker3D GetSleepingPosition() => GetNode<Marker3D>("SleepPosition");

		public void ShowInteractPrompt()
		{
			//OverheadManager.ShowInteractPrompt();
			GetTree().Root.GetNode<GUI>("World/GUI").ShowInteractPrompt("Sleep [E]");
		}

		public void HideInteractPrompt()
		{
			//OverheadManager.HideInteractPrompt();
			GetTree().Root.GetNode<GUI>("World/GUI").HideInteractPrompt();
		}

		public void OnInteract(Player player)
		{
			player.Sleep();
		}

		public override void _Ready()
		{
			base._Ready();
			OverheadManager = new OverheadManager(this, "Rest [E]");
		}
	}
}
