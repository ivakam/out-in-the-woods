using Godot;
using System;

namespace outinthewoods
{
	public partial class Note : StaticBody3D,IInteractable
	{
		[Export] private NodePath noteGui;
		
		public Node3D Thing()
		{
			return this;
		}

		public OverheadManager OverheadManager { get; }
		public void ShowInteractPrompt()
		{
			GetTree().Root.GetNode<GUI>("World/GUI").ShowInteractPrompt("Read [E]");
		}

		public void HideInteractPrompt()
		{
			GetTree().Root.GetNode<GUI>("World/GUI").HideInteractPrompt();
		}

		public void OnInteract(Player player)
		{
			GetNode<NoteGui>(noteGui).Open();
		}
	}
}
