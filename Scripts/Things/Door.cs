using Godot;
using System;

namespace outinthewoods
{
	public partial class Door : AnimatableBody3D,IInteractable
	{
		public OverheadManager OverheadManager { get; private set; }
		private bool _open = false;
		private bool _locked = false;
		
		public Node3D Thing()
		{
			return this;
		}

		public void ShowInteractPrompt()
		{
			//OverheadManager.ShowInteractPrompt();
			GetTree().Root.GetNode<GUI>("World/GUI").ShowInteractPrompt("Open/Close [E]");
		}

		public void HideInteractPrompt()
		{
			//OverheadManager.HideInteractPrompt();
			GetTree().Root.GetNode<GUI>("World/GUI").HideInteractPrompt();
		}

		public void Open()
		{
			if (_open || _locked)
				return;
			_open = true;
			AnimationPlayer ani = GetNode<AnimationPlayer>("AnimationPlayer");
			ani.Play("Open");
		}

		public void Lock()
		{
			Close();
			_locked = true;
		}

		public void Unlock()
		{
			_locked = false;
		}

		public void Close()
		{
			if (!_open)
				return;
			_open = false;
			AnimationPlayer ani = GetNode<AnimationPlayer>("AnimationPlayer");
			ani.Play("Close");
		}

		public void OnInteract(Player player)
		{
			AnimationPlayer ani = GetNode<AnimationPlayer>("AnimationPlayer");

			if (!_open)
				Open();
			else
				Close();
		}

		public string Title { get; } = "Door";

		public override void _Ready()
		{
			base._Ready();
			OverheadManager = new OverheadManager(this, "Open/Close [E]");
		}
	}
}
