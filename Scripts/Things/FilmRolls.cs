using Godot;

namespace outinthewoods
{
    public partial class FilmRolls : StaticBody3D,IInteractable
    {
        public Node3D Thing()
        {
            return this;
        }

        public OverheadManager OverheadManager { get; }
        public void ShowInteractPrompt()
        {
			GetTree().Root.GetNode<GUI>("World/GUI").ShowInteractPrompt("Restock film [E]");
        }

        public void HideInteractPrompt()
        {
			GetTree().Root.GetNode<GUI>("World/GUI").HideInteractPrompt();
        }

        public void OnInteract(Player player)
        {
            player.RestockFilm();
        }
    }
}
