using System.Collections.Generic;
using Godot;

namespace outinthewoods
{
    /// <summary>
    ///     Interface for all "things". Acts as a way to attach extra behaviour to Node3Ds.
    /// </summary>
    public interface IThing
    {
        public Node3D Thing();
    }

    public interface IInteractable : IThing
    {
        public OverheadManager OverheadManager { get; }
        public void ShowInteractPrompt();
        public void HideInteractPrompt();
        public void OnInteract(Player player);
    }

    public interface IPocketable : IInteractable
    {
        public string Description { get; }
        public bool Pocketable { get; }
        public bool CanBeTaken();
    }

    public interface IPhotoable
    {
        public string Title { get; }
    }

    public interface IGui
    {
        public bool Close();
        public void Open();
    }
}