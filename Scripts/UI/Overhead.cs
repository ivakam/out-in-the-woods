using Godot;

namespace outinthewoods
{
	public partial class Overhead : Sprite3D
	{
		private string _text;

		public void Setup(string text)
		{
			GD.Print(text);
			_text = text;
		}

		public override void _Ready()
		{
			var view = (Viewport) GetNode("GUIContainer/GUI");
			var vbox = (VBoxContainer) view.GetNode("VBox");

			vbox.GetNode<Label>("Text").Text = _text;

			var tex = view.GetTexture();
			Texture = tex;
		}
	}
}
