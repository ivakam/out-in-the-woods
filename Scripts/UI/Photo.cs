using Godot;

namespace outinthewoods
{
	public partial class Photo : TextureRect
	{
		private static readonly PackedScene photoScene = ResourceLoader.Load<PackedScene>("res://Scenes/UI/Photo.tscn");

		public static Photo Create(string entity, Image img)
		{
			Photo res = photoScene.Instantiate<Photo>();
			res.Entity = entity;
			ImageTexture tex = ImageTexture.CreateFromImage(img);
			res.Texture = tex;
			return res;
		}

		public static Photo Create(Photo photo)
		{
			Photo res = photoScene.Instantiate<Photo>();
			res.Entity = photo.Entity;
			res.Texture = photo.Texture;
			return res;
			
		}
		
		public string Entity { get; private set; } = "Unknown";
		
		private void OnPhotoClicked(InputEvent e)
		{
			if (e is InputEventMouseButton b)
			{
				if (b.ButtonIndex == MouseButton.Left && !b.Pressed)
				{
					GetTree().Root.GetNode<GUI>("World/GUI").UpdateOrAddEntityPage(Create(this));
				}
			}
		}
	}
}
