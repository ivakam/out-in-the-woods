using Godot;
using System.Collections.Generic;
using System.Linq;

namespace outinthewoods
{
	public partial class JournalGui : Control,IGui
	{
		private TabContainer _pageContainer;
		private List<Page> _pages;
		private AnimatedSprite2D _book;
		private bool _typing;
		private int _lastFilledPage = -1;
		
		public bool Close()
		{
			bool wasOpen = Visible;
			Hide();
			Input.MouseMode = Input.MouseModeEnum.Captured;
			return wasOpen;
		}

		public void Open()
		{
			UpdatePageNumber();
			Visible = true;
			Input.MouseMode = Input.MouseModeEnum.Visible;
		}

		public void UpdateOrAddEntityPage(Photo photo)
		{
			int page = _pages.FindIndex((p) => p.Entity == photo.Entity);
			if (page == -1)
			{
				page = _lastFilledPage + 1;
				_lastFilledPage++;
			}
			_pages[page].Entity = photo.Entity;
			SetPhoto(page, photo);
		}

		public string GetPlayerEntityName(string entity)
		{
			int page = _pages.FindIndex(p => p.Entity == entity);
			if (page == -1)
				return "Unknown";
			return _pages[page].PlayerName();
		}

		protected void SetPhoto(int index, Photo p)
		{
			_pages[index].SetPhoto(p);
		}

		private void OnTyping()
		{
			_typing = true;
		}

		private void OnStopTyping()
		{
			_typing = false;
			_pages[_pageContainer.CurrentTab].CheckCompletion();
		}

		private void OnPrevPagePressed()
		{
			if (_pageContainer.CurrentTab == 0)
				return;
			_book.Play("prev");
			GetNode<AudioStreamPlayer>("PageFlipSound").Play();
			GetNode<Control>("PageContainer").Visible = false;
		}

		private void OnNextPagePressed()
		{
			if (_pageContainer.CurrentTab == _pageContainer.GetTabCount() - 1)
				return;
			_book.Play("next");
			GetNode<AudioStreamPlayer>("PageFlipSound").Play();
			GetNode<Control>("PageContainer").Visible = false;
		}

		private void UpdatePageNumber()
		{
			GetNode<Label>("PageContainer/PageNumber").Text = 
				$"{_pageContainer.CurrentTab + 1}/{_pageContainer.GetTabCount()}";
		}
		
		private void OnPageFlipAnimationFinished()
		{
			switch (_book.Animation)
			{
				case "next":
					_pageContainer.CurrentTab += 1;
					break;
				case "prev":
					_pageContainer.CurrentTab -= 1;
					break;
			}

			_book.Stop();
			_book.Frame = 0;
			UpdatePageNumber();
			GetNode<Control>("PageContainer").Visible = true;
		}

		public override void _PhysicsProcess(double delta)
		{
			base._PhysicsProcess(delta);
			if (!Visible || _typing)
				return;
			if (Input.IsActionJustPressed("move_right"))
				OnNextPagePressed();
			if (Input.IsActionJustPressed("move_left"))
				OnPrevPagePressed();
		}

		public override void _Ready()
		{
			base._Ready();
			_pageContainer = GetNode<TabContainer>("PageContainer/Pages");
			_pages = _pageContainer.GetChildren().Cast<Page>().ToList();
			_book = GetNode<AnimatedSprite2D>("PageFlip");
			
			foreach (Control c in GetTree().GetNodesInGroup("typing"))
			{
				c.Connect("focus_entered", new Callable(this, nameof(OnTyping)));
				c.Connect("focus_exited", new Callable(this, nameof(OnStopTyping)));
			}
		}
	}
}




