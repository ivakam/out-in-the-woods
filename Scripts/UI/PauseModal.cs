using Godot;
using System;

namespace outinthewoods
{
	public partial class PauseModal : Control,IGui
	{
		public bool Close()
		{
			bool wasOpen = Visible;
			Hide();
			GetTree().Paused = false;
			Input.MouseMode = Input.MouseModeEnum.Captured;
			return wasOpen;
		}

		public void Open()
		{
			Show();
			GetTree().Paused = true;
			Input.MouseMode = Input.MouseModeEnum.Visible;
		}
	}
}
