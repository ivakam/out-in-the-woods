using Godot;
using System;

namespace outinthewoods
{
    public partial class PhotoManager : MarginContainer,IGui
    {
        public bool Close()
        {
            bool wasOpen = Visible;
            Hide();
            Input.MouseMode = Input.MouseModeEnum.Captured;
            return wasOpen;
        }

        public void Open()
        {
            Input.MouseMode = Input.MouseModeEnum.Visible;
            Show();
        }
    }
}
