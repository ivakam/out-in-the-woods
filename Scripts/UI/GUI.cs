using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

namespace outinthewoods
{
	public partial class GUI : Control
	{
		private static readonly PackedScene AlbumRow = ResourceLoader.Load<PackedScene>("Scenes/UI/AlbumRow.tscn");

		public void ResetOverlays()
		{
			UpdateStaticOverlay(10000);
			GetNode<ColorRect>("DeathOverlay").Hide();
		}
		
		public void ShowPhotoManager(Player player)
		{
			PhotoManager photoManager = GetNode<PhotoManager>("PhotoManager");
			VBoxContainer album = photoManager.GetNode<VBoxContainer>("Panel/VBox/VScroll/Album");
			Dictionary<string,List<Photo>> roll = player.CameraRoll.ToDict();
			
			// Clear existing rows
			foreach (Node child in album.GetChildren())
			{
				child.QueueFree();
			}
			
			foreach (string entity in roll.Keys)
			{
				HBoxContainer row = AlbumRow.Instantiate<HBoxContainer>();

				string name = GetNode<JournalGui>("Journal").GetPlayerEntityName(entity);
				
				row.GetNode<Label>("Name").Text = name;
				
				// Clear placeholder images
				foreach (Node n in row.GetNode<HBoxContainer>("HScroll/Photos").GetChildren())
				{
					n.QueueFree();
				}
				
				// Populate the rows with the pictures belonging to each entity in the camera roll dict
				for (int i = 0; i < CameraRoll.Capacity; i++)
				{
					if (roll[entity].Count <= 0)
						break;
					Photo p = roll[entity].First();
					row.GetNode<HBoxContainer>("HScroll/Photos").AddChild(p);
					roll[entity].Remove(p);
					p.Connect("gui_input", new Callable(p, "OnPhotoClicked"));
				}
				album.AddChild(row);
			}
			
			photoManager.Open();
		}
		
		public void ViewJournal()
		{
			GetNode<JournalGui>("Journal").Open();
		}

		public void ShowNote()
		{
			GetNode<NoteGui>("Note").Show();
		}
		
		public void ShowSleepPrompt()
		{
			GetNode<RestModal>("RestModal").Open();
		}

		public void HideSleepPrompt()
		{
			GetNode<RestModal>("RestModal").Close();
		}

		public void ShowPauseMenu()
		{
			GetNode<PauseModal>("PauseModal").Open();
		}

		public void HidePauseMenu()
		{
			GetNode<PauseModal>("PauseModal").Close();
		}

		public void ShowInteractPrompt(string prompt)
		{
			GetNode<Label>("InteractBox/Label").Text = prompt;
			GetNode<HBoxContainer>("InteractBox").Visible = true;
		}

		public void HideInteractPrompt()
		{
			GetNode<HBoxContainer>("InteractBox").Visible = false;
		}

		public void ToggleCameraOverlay()
		{
			TextureRect viewFinder = GetNode<TextureRect>("ViewFinder");
			viewFinder.Visible = !viewFinder.Visible;
		}

		public void UpdateRemainingFilm(int film, int max)
		{
			Label filmStock = GetNode<Label>("ViewFinder/FilmStock");
			filmStock.Text = $"{film}/{max}";
		}

		public void UpdateOrAddEntityPage(Photo photo)
		{
			GetNode<AudioStreamPlayer>("PhotoSelect").Play();
			GetNode<JournalGui>("Journal").UpdateOrAddEntityPage(photo);
		}

		public void UpdateStaticOverlay(float distance)
		{
			float alpha = (1 - distance / 30) * 0.3f;
			Control overlay = GetNode<Control>("StaticOverlay");
			overlay.Modulate = new Color(1, 1, 1, alpha);
		}

		public void UpdateDebug(double timer, string encounter)
		{
			VBoxContainer debugBox = GetNode<VBoxContainer>("DebugPanel/DebugVBox");
			debugBox.GetNode<Label>("TimerLabel").Text = $"Timer: {Math.Round(timer, 2)}";
			debugBox.GetNode<Label>("EncounterLabel").Text = $"Encounter: {encounter}";
		}

		private bool CloseWindows()
		{
			bool closedAny = false;
			foreach (Node n in GetChildren())
			{
				if (n is IGui g)
				{
					if (g.Close())
					{
						closedAny = true;
					}
				}
			}

			return closedAny;
		}

		public override void _PhysicsProcess(double delta)
		{
			if (Input.IsActionJustPressed("close"))
			{
				if (!CloseWindows())
				{
					ShowPauseMenu();
				}
			}

			if (Input.IsActionJustPressed("debug"))
			{
				GetNode<ColorRect>("DebugPanel").Visible = !GetNode<ColorRect>("DebugPanel").Visible;
			}
				
		}
		
		/*
		 * Signal handlers
		 */

	}
}
