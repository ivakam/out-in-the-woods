using Godot;

namespace outinthewoods
{
    public class OverheadManager
    {
        private readonly PackedScene _overhead = ResourceLoader.Load<PackedScene>("res://Scenes/UI/Overhead.tscn");
        protected Overhead Overhead { get; set; }
        protected IInteractable Owner { get; set; }
        protected string Text { get; set; }

        public OverheadManager(IInteractable owner, string text)
        {
            Owner = owner;
            Text = text;
        }
        
        public void ShowInteractPrompt()
        {
            GD.Print("showing prompt");
            if (Overhead == null || Overhead.IsQueuedForDeletion())
            {
                Overhead = _overhead.Instantiate<Overhead>();
                Overhead.Name = "Overhead";
                Owner.Thing().AddChild(Overhead);
                Overhead.Setup(Text);
            }
        }

        public void HideInteractPrompt()
        {
            GD.Print("hiding prompt");
            if (Overhead == null || Overhead.IsQueuedForDeletion())
                return;
            Overhead.QueueFree();
            Overhead = null;
        }
    }
}