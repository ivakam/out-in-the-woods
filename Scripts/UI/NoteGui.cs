using Godot;
using System;

namespace outinthewoods
{
    public partial class NoteGui : TextureRect,IGui
    {
        public bool Close()
        {
            bool wasOpen = Visible;
            Hide();
            Input.MouseMode = Input.MouseModeEnum.Captured;
            return wasOpen;
        }

        public void Open()
        {
            Visible = true;
            Input.MouseMode = Input.MouseModeEnum.Visible;
        }
    }
}
