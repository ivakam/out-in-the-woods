using Godot;
using System;

namespace outinthewoods
{
	public partial class Page : HBoxContainer
	{
		public string Entity { get; set; } = null;
		public bool IsComplete { get; private set; }

		private LineEdit _name;
		private LineEdit _height;
		private TextEdit _time;
		private TextEdit _appearance;
		private TextEdit _behavior;
		private HBoxContainer _portraitBox;
		private Photo _photo;

		public string PlayerName()
		{
			return _name.Text;
		}
		
		public void SetPhoto(Photo p)
		{
			foreach(Node n in _portraitBox.GetChildren())
				n.QueueFree();
			_portraitBox.AddChild(p);
		}

		public bool CheckCompletion()
		{
			if (Entity == null)
				return false;
			bool complete =  ActorInfo.GetActorInfoFromName(Entity).IsFilledOut(
				_appearance.Text,
				_behavior.Text,
				_time.Text,
				_height.Text);

			if (complete && !IsComplete)
			{
				IsComplete = true;
				GetNode<TextureRect>("Right/MarkerContainer/CompleteMarker").Show();
			}
			return complete;
		}

		public override void _Ready()
		{
			base._Ready();
			_name = GetNode<LineEdit>("Left/NameBox/Name");
			_height = GetNode<LineEdit>("Left/InfoBox/HeightBox/Height");
			_time = GetNode<TextEdit>("Left/InfoBox/TimeBox/Time");
			_appearance = GetNode<TextEdit>("Right/Appearance");
			_behavior = GetNode<TextEdit>("Right/Behavior");
			_portraitBox = GetNode<HBoxContainer>("Left/PortraitBox");
		}
	}
}
