using Godot;
using System;

namespace outinthewoods
{
    public partial class LookAtArea : Area3D
    {
        [Signal] public delegate void LookedAtEventHandler();
        public void NotifyLookedAt()
        {
            EmitSignal(nameof(LookedAtEventHandler));
        }
    }
}
