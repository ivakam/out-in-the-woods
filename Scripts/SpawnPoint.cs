using Godot;
using System;

namespace outinthewoods
{
	public partial class SpawnPoint : Marker3D
	{
		public override void _Ready()
		{
			base._Ready();
			RayCast3D ray = GetNode<RayCast3D>("GroundRay");
			while (!ray.IsColliding())
			{
				Translate(Vector3.Down * 0.15f);
				ray.ForceRaycastUpdate();
			}

			ray.Enabled = false;
		}

		private void OnSpawnPointVisible()
		{
			RemoveFromGroup("spawn_point");
		}

		private void OnSpawnPointNotVisible()
		{
			AddToGroup("spawn_point");
		}
	}
}
